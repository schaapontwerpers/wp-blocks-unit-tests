<?php

namespace BlockUnitTest;

use WordPressPluginAPI\Manager;

class Init
{
    /**
     * Contains all post types and taxonomies that need to be registered.
     */
    private $hooks;

    /**
     * Contains all classes to register with the plugin API
     */
    private $registers;

    /**
     * Constructor
     *
     * Initializes all classes
     * Runs the register function
     * Registers all shortcodes
     */
    public function __construct()
    {
        $this->hooks = [
            new RestAPI\PostTypes\Pages(),
        ];

        // Register post types and taxonomies
        $this->registers = [
            new PostTypes\UnitTests(),
        ];

        // Run PluginAPI registration
        $manager = new Manager();
        foreach ($this->hooks as $class) {
            $manager->register($class);
        }

        foreach ($this->registers as $register) {
            $register->register();
        }
    }
}
