<?php

namespace Jabbado\RestAPI\Helpers;

class Params
{
    public $args = [];

    private $hasParentSlug = false;

    private $taxonomies = [];

    private $request;

    public function __construct(array $args, \WP_REST_Request $request)
    {
        $this->args = $args;
        $this->request = $request;

        $this->hasParams();
    }

    /**
     * Set correct order
     */
    private function hasParams()
    {
        $this->hasParentSlug = $this->request->has_param('parentSlug');
    }

    /**
     * Add parent to WP Query arguments
     */
    public function setPostParent($type)
    {
        if ($this->hasParentSlug) {
            $parentSlug = $this->request->get_param('parentSlug');
            $page = get_page_by_path(
                $parentSlug,
                OBJECT,
                $type
            );

            if ($page) {
                $this->args['post_parent'] = $page->ID;
            }
        }
    }
    /**
     * Set search query
     *
     * @param array $args Aguments to add
     *
     * @return void
     */
    public function setArgs(array $args)
    {
        $this->args = array_merge($this->args, $args);
    }

    /**
     * Bind all taxonomies to the WP Query arguments
     *
     * @param array $default Default tax query
     *
     * @return void
     */
    public function setTaxQuery(array $default = [])
    {
        $taxonomies = $default;

        foreach ($this->taxonomies as $taxonomy => $value) {
            array_push(
                $taxonomies,
                [
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => explode(',', $value),
                    'operator' => 'AND',
                ],
            );
        }

        // Add taxonomies if it's filled
        if (count($taxonomies) > 0) {
            if (count($taxonomies) > 1) {
                $query = [
                    'relation' => 'AND',
                ];

                foreach ($taxonomies as $tax) {
                    array_push($query, $tax);
                }
            } else {
                $query = [$taxonomies[0]];
            }

            $this->args['tax_query'] = $query;
        }
    }
}
