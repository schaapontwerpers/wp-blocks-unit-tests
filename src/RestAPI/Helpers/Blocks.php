<?php

namespace Jabbado\RestAPI\Helpers;

use Jabbado\CustomFields;

class Blocks
{
    public $blocks;

    private $postId;

    private $request;

    /**
     * Constructor
     */
    public function __construct(
        int $postId,
        string $postType,
        \WP_REST_Request $request,
        array $blocks,
    ) {
        $this->blocks = $this->convertBlocks($blocks);
        $this->postId = $postId;
        $this->request = $request;
    }


    /**
     * Convert reusable blocks to the right block data
     */
    private function convertBlocks(array $blocks): array
    {
        $convertedBlocks = [];

        foreach ($blocks as $block) {
            $convertedBlocks = array_merge(
                $convertedBlocks,
                $this->convertBlock($block)
            );
        }

        return $convertedBlocks;
    }

    /**
     * Convert a single reusable block to the right block data
     */
    private function convertBlock(array $block): array
    {
        $convertedBlocks = [];

        if ($block['blockName'] === 'core/block') {
            $blockId = $block['attrs']['ref'];
            $post = get_post($blockId);

            if ($post) {
                $parsedBlocks = parse_blocks($post->post_content);

                // Clean all innerBlocks aswell
                foreach ($parsedBlocks as $parsedBlock) {
                    $convertedBlock = $this->convertBlock($parsedBlock);

                    if (array_key_exists('innerBlocks', $convertedBlock)) {
                        $innerBlocks = [];

                        foreach ($convertedBlock['innerBlocks'] as $innerBlock) {
                            $innerBlocks = array_merge(
                                $innerBlocks,
                                $this->convertBlock($innerBlock),
                            );
                        }

                        $convertedBlock['innerBlocks'] = $innerBlocks;
                    }

                    $convertedBlocks = array_merge($convertedBlocks, $convertedBlock);
                }
            }
        } else {
            $convertedBlock = $block;

            if (array_key_exists('innerBlocks', $convertedBlock)) {
                $innerBlocks = [];

                foreach ($convertedBlock['innerBlocks'] as $innerBlock) {
                    $innerBlocks = array_merge(
                        $innerBlocks,
                        $this->convertBlock($innerBlock)
                    );
                }

                $convertedBlock['innerBlocks'] = $innerBlocks;
            }

            array_push($convertedBlocks, $convertedBlock);
        }

        return $convertedBlocks;
    }

    /**
     * Add dynamic attributes to a block
     */
    public function clean(): void
    {
        $cleanBlocks = [];

        foreach ($this->blocks as $key => $parseBlock) {
            if ($parseBlock['blockName'] !== null) {
                $cleanBlocks[] = $this->cleanBlock($parseBlock);
            }
        }

        $this->blocks = $cleanBlocks;
    }

    /**
     * Clean Gutenberg blocks from unnecessary data
     */
    private function cleanBlock(array $block): array
    {
        // Add additional data
        $block = $this->addData($block);
        $block['id'] = wp_unique_id('block-');

        // Check if block has it's own blocks inside
        if (count($block['innerBlocks']) > 0) {
            $innerBlocks = [];

            // Clean all innerBlocks aswell
            foreach ($block['innerBlocks'] as $innerBlock) {
                $innerBlocks[] = $this->cleanBlock($innerBlock);
            }

            $block['innerBlocks'] = $innerBlocks;
        } else {
            unset($block['innerBlocks']);
        }

        // Always return attributes as array
        if (!$block['attrs']) {
            $block['attrs'] = (object) array();
        }

        unset($block['innerHTML']);
        unset($block['innerContent']);

        return $block;
    }

    /**
     * Add dynamic attributes to a block
     */
    private function addData(array $block): array
    {
        switch ($block['blockName']) {
            case 'jabbado/overview':
                $block = $this->populateOverview($block);

                break;

            case 'jabbado/editorial':
                $block = $this->populateEditorial($block);

                break;

            default:
                break;
        }

        return $block;
    }

    private function populateEditorial(array $block): array
    {
        if (array_key_exists('postId', $block['attrs'])) {
            $customFields = new CustomFields\PostTypes();

            $block['attrs']['data'] = $customFields->filterPost($block['attrs']['postId']);
        }

        return $block;
    }

    /**
     * Add dynamic attributes to a block
     */
    private function populateOverview(array $block): array
    {
        $customFields = new CustomFields\PostTypes();

        $block['attrs']['initialData'] = $customFields->getPosts(
            $block['attrs'],
            $this->request,
        );

        if (
            array_key_exists('addFilters', $block['attrs'])
            && array_key_exists('taxonomyFilters', $block['attrs'])
        ) {
            $customFields = new CustomFields\Taxonomies();

            if ($block['attrs']['addFilters']) {
                $block['attrs']['initialFilterData']
                    = $customFields->getTaxonomies(
                        $block['attrs']['taxonomyFilters'],
                    );
            }
        }

        return $block;
    }
}
