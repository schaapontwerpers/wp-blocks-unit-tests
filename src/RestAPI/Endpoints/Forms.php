<?php

namespace Jabbado\RestAPI\Endpoints;

use EasyDOM\DOMDocument;
use WordPressClassHelpers\Register\RestRoute;

class Forms extends RestRoute
{
    private $body;

    private $code;

    private $emails = '';

    private $referer;

    private $message;

    private $DOM;

    private $success = true;

    private $statusCode = 201;

    protected $methods = \WP_REST_SERVER::CREATABLE;

    protected function setRoute()
    {
        $this->route = 'forms';
    }

    /**
     * Build the callback
     */
    public function getCallback(\WP_REST_Request $request)
    {
        $response = [];

        $this->referer = $request->get_header('referer');
        $this->body = $request->get_json_params();

        $this->verifyAndBuild();

        if ($this->success) {
            $this->sendMail();

            if ($this->emails !== '') {
                $this->sendMailToClients();
            }

            $response = new \WP_REST_Response(
                [
                    'message' => $this->message,
                    'success' => $this->success,
                ],
            );
            $response->set_status($this->statusCode);
        } else {
            $response = new \WP_Error(
                $this->code,
                $this->message,
                [
                    'status' => $this->statusCode,
                ],
            );
        }

        return rest_ensure_response($response);
    }

    /**
     * Get the permission callback
     */
    public function getPermissionCallback(): bool
    {
        return true;
    }

    /**
     * Verify and build HTML response
     */
    private function verifyAndBuild()
    {
        $this->buildHTML();
    }

    /**
     * Build the HTML message
     */
    private function buildHTML()
    {
        // Build DomDocument
        $this->DOM = new DOMDocument();

        // Fill table with POST data
        $table = $this->createTable();

        // Build DOM message
        $html = $this->DOM->createElement('html');
        $body = $this->DOM->createElement('body');
        $body->appendChild($table);
        $html->appendChild($body);

        // Save DOM to message
        $this->DOM->appendChild($html);
    }

    /**
     * Send the form
     */
    private function createTable()
    {
        // Create table element
        $atts = array(
            'rules' => 'all',
            'cellpadding' => '0',
            'width' => '100%',
            'style' => 'max-width: 600px; margin: 0 auto;',
        );
        $table = $this->DOM->generateElement('table', $atts);
        $tbody = $this->DOM->createElement('tbody');
        $table->appendChild($tbody);

        $i = 0;

        foreach ($this->body['inputs'] as $field) {
            $label = $field['showExtraLabel'] ?
                $this->stripString($field['extraLabel']) :
                $this->stripString($field['label']);
            $value = $this->stripString($field['value']);
            $type = $field['type'];
            $req = $field['required'];

            // Validate field
            if ($req) {
                $this->validateField($label, $value, $type);
            }

            // Only continue building if there are no errors yet
            if ($this->success) {
                $tr = ($type == 'checkbox') ?
                    $this->createRow($type, $label, $value === '' ? 'Uitgevinkt' : 'Aangevinkt', $i) :
                    $this->createRow($type, $label, $value, $i);

                $tbody->appendChild($tr);

                $i++;
            } else {
                break;
            }
        }

        // Add referer to table
        if ($this->referer) {
            $tr = $this->createRow(
                'text',
                __('URL', 'jabbado'),
                $this->referer,
                $i
            );
            $tbody->appendChild($tr);
        }

        return $table;
    }

    /**
     * Create a row for the table
     */
    private function createRow(
        string $type,
        string $label,
        string $value,
        int $i
    ) {
        $backgroundColor = ($i % 2 == 0) ? '#fefefe' : '#fff';

        $atts = array('style' => 'background-color: ' . $backgroundColor . ';');
        $tr = $this->DOM->generateElement('tr', $atts);

        $atts = array(
            'align' => 'left',
            'style' => 'border-color: transparent; padding: 9px 36px 9px 0;',
            'valign' => 'top'
        );
        $th = $this->DOM->generateElement('th', $atts);
        $tr->appendChild($th);

        $strong = $this->DOM->generateElement(
            'strong',
            null,
            $label
        );
        $th->appendChild($strong);

        $atts = array(
            'align' => 'left',
            'style' => 'border: 0; padding: 9px 0 9px 0; width: 60%;',
            'valign' => 'top'
        );
        $td = $this->DOM->generateElement('td', $atts, $value);
        $tr->appendChild($td);

        return $tr;
    }

    /**
     * Validate a single field
     */
    private function validateField(
        string $label,
        string $value,
        string $type
    ) {
        // translators: Placeholder is name of the field
        $error = __('%s has not passed validation.', 'jabbado');

        // Check if the field is not empty
        if (strlen($value) == 0 || ctype_space($value)) {
            // Set response to error message
            $this->message = sprintf($error, $label);
            $this->success = false;
            $this->statusCode = 400;
            $this->code = 'validation_failed';
        }

        // Type specific checks
        switch ($type) {
                // Mail fields
            case 'email':
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    // Set response to error message
                    $this->message = sprintf($error, $label);
                    $this->success = false;
                    $this->statusCode = 400;
                    $this->code = 'validation_failed';
                } else {
                    $this->emails .= (strlen($this->emails) > 0) ?
                        ',' . $value :
                        $value;
                }

                break;

            case 'number':
                if (!is_numeric($value)) {
                    // Set response to error message
                    $this->message = sprintf($error, $label);
                    $this->success = false;
                    $this->statusCode = 400;
                    $this->code = 'validation_failed';
                }

                break;

            default:
                break;
        }
    }

    /**
     * Strip input value
     */
    private function stripString(string $string): string
    {
        $string = strip_tags($string);
        $string = trim($string);
        $string = stripslashes($string);
        $string = htmlspecialchars($string);

        return $string;
    }

    /**
     * Send the e-mail
     */
    private function sendMail()
    {
        $mailData = $this->body['mail'];

        if ($mailData['send']) {
            // Get list of receivers
            $to = $mailData['to'];

            // Text and subject
            $subject = $this->body['subject'];

            $headers = array(
                'From: ' . MAIL_FROM_NAME . ' <' . MAIL_FROM . '>',
                'MIME-Version: 1.0',
                'Content-Type: text/html; charset=UTF-8',
            );

            // Send mail
            $mail = wp_mail($to, $subject, $this->DOM->saveHTML(), $headers);
            $this->message = __('Form sent succesfully!', 'jabbado');
        }
    }

    /**
     * Send the e-mail
     */
    private function sendMailToClients()
    {
        $mailToClient = $this->body['mailToClient'];

        if (array_key_exists('send', $mailToClient) && $mailToClient['send']) {
            // Get list of receivers
            $to = $this->emails;

            // Text and subject
            $subject = array_key_exists('subjectToClient', $mailToClient) ?
                $mailToClient['subjectToClient'] :
                'Bevestiging ontvangst formulier';

            $headers = array(
                'From: ' . MAIL_FROM_NAME . ' <' . MAIL_FROM . '>',
                'MIME-Version: 1.0',
                'Content-Type: text/html; charset=UTF-8',
            );

            $message = '';

            if (array_key_exists('addMailMessage', $mailToClient) && $mailToClient['addMailMessage']) {
                if (array_key_exists('message', $mailToClient)) {
                    $message .= nl2br($mailToClient['message']) . '<br /><br />';
                }
            }

            if (array_key_exists('addMailForm', $mailToClient) && $mailToClient['addMailForm']) {
                $message .= $this->DOM->saveHTML();
            }

            // Send mail
            wp_mail(
                $to,
                $subject,
                $message,
                $headers,
            );
            $this->message = __('Form sent succesfully!', 'jabbado');
        }
    }
}
