<?php

namespace Jabbado\RestAPI\Endpoints;

use Jabbado\RestAPI\Helpers\Blocks;
use WordPressClassHelpers\Register\RestRoute;

class App extends RestRoute
{
    protected function setRoute()
    {
        $this->route = 'app';
    }

    /**
     * Build the callback
     */
    public function getCallback(\WP_REST_Request $request)
    {
        $response = new \WP_REST_Response(
            [
                'menu' => $this->getMenu('primary'),
                'footer' => $this->getFooter($request),
            ]
        );

        return rest_ensure_response($response);
    }

    /**
     * Get the permission callback
     */
    public function getPermissionCallback(): bool
    {
        return true;
    }

    /**
     * Get footer data
     */
    private function getFooter(\WP_REST_Request $request): array
    {
        $returnBlocks = [];
        $data = get_page_by_path('footer', OBJECT, 'structure');

        if ($data) {
            $ID = $data->ID;
            $content = get_post_field('post_content', $ID);

            $parseBlocks = parse_blocks($content);
            $blocks = new Blocks(
                $ID,
                'structure',
                $request,
                $parseBlocks,
            );
            $blocks->clean();

            $returnBlocks = $blocks->blocks;
        }

        return $returnBlocks;
    }

    /**
     * Get a menu
     */
    private function getMenu(string $slug): array
    {
        $restMenu = array();

        // Get menu locations
        $locs = get_nav_menu_locations();

        // Check if paramater slug exists as location
        if (array_key_exists($slug, $locs)) {
            $menu = get_term($locs[$slug], 'nav_menu');

            if ($menu) {
                // Generate REST items from menu items
                $items = wp_get_nav_menu_items($menu->slug);
                $restMenuItems = array();

                foreach ($items as $item) {
                    $restMenuItems[] = $this->getMenuItem($item);
                }

                // Fill REST menu array with menu properties
                $restMenu['name'] = $menu->name;
                $restMenu['items'] = $this->buildTree($restMenuItems, 0);
            }
        }

        return $restMenu;
    }

    /**
     * Build a multidimensional array based on parent ID
     */
    private function buildTree(array &$elements, int $parentId = 0)
    {
        $branch = array();

        foreach ($elements as &$element) {
            if (array_key_exists('parent', $element)) {
                if ($element['parent'] == $parentId) {
                    $children = $this->buildTree($elements, $element['id']);

                    if ($children) {
                        $element['children'] = $children;
                    }

                    $branch[] = $element;
                    unset($element);
                }
            }
        }

        return $branch;
    }

    /**
     * Format a menu item for REST API consumption
     */
    private function getMenuItem(\WP_Post $item): array
    {
        $addExpertises = get_field('addExpertises', $item);
        $addImage = get_field('image', $item);

        $restMenuItem = array(
            'id' => (int)$item->ID,
            'name' => htmlspecialchars_decode($item->title),
            'url' => str_replace(WP_HOME, '', $item->url),
            'parent' => (int)$item->menu_item_parent,
            'image' => $addImage ? $addImage : null,
            'expertises' => $addExpertises ? $this->getExpertise() : null,
        );

        return $restMenuItem;
    }
    /**
     * Get all expertises for the menu
     */
    private function getExpertise(): array
    {
        $args = [
            'fields' => 'ids',
            'post_type' => 'expertise',
            'posts_per_page' => -1,
            'orderby' => 'post_title',
            'order' => 'ASC',
        ];
        $query = new \WP_Query($args);
        $expertises = [];

        if ($query->have_posts()) {
            foreach ($query->posts as $id) {
                $expertises[] = [
                    'title' => htmlspecialchars_decode(get_the_title($id)),
                    'link' => str_replace(WP_HOME, '', get_permalink($id)),
                ];
            }
        }

        return $expertises;
    }
}
