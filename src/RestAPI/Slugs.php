<?php

namespace Jabbado\RestAPI;

class Slugs
{
    /**
     * All post type slugs
     */
    public $postTypeSlugs;

    /**
     * All taxonomy slugs
     */
    public $taxonomySlugs;

    /**
     * Get the paramters
     */
    public function __construct()
    {
        $this->postTypeSlugs = $this->getPostTypeSlugs();
        $this->taxonomySlugs = $this->getTaxonomySlugs();
    }

    /**
     * Get all public post type slugs
     */
    public function getPostTypeSlugs(): array
    {
        $postTypes = [];
        $postTypeObjects = get_post_types(
            [
                'public' => true,
                'exclude_from_search' => false,
            ],
            'object',
        );

        foreach ($postTypeObjects as $postTypeObject) {
            $slug = $postTypeObject->name;

            // Continue if the slug is page or attachment
            if ($slug === 'attachment') {
                continue;
            }

            $postTypes[] = [
                'component' => $this->getComponentName($slug),
                'permalink' => $this->getPermalink($postTypeObject),
                'restBase' => $postTypeObject->rest_base,
                'slug' => $slug,
                'title' => $postTypeObject->labels->name,
            ];
        }

        return $postTypes;
    }

    /**
     * Get all public post type slugs
     */
    public function getTaxonomySlugs(): array
    {
        $taxonomies = [];
        $taxonomyObjects = get_taxonomies(
            [
                'public' => true,
            ],
            'object',
        );

        foreach ($taxonomyObjects as $taxonomyObject) {
            $slug = $taxonomyObject->name;

            $permalink = $taxonomyObject->rewrite ?
                $taxonomyObject->rewrite['slug'] :
                $slug;

            $taxonomies[] = [
                'slug' => $slug,
            ];
        }

        return $taxonomies;
    }

    /**
     * Get the corresponding React Component
     */
    private function getComponentName(string $slug): string
    {
        $splitSlug = explode('-', $slug);
        $componentParts = [];

        foreach ($splitSlug as $slugPart) {
            $componentParts[] = ucfirst($slugPart);
        }

        return implode('', $componentParts);
    }

    /**
     * Get the correct permalink structure
     */
    private function getPermalink(object $postTypeObject): string
    {
        global $wp_rewrite;

        $slug = $postTypeObject->name;
        $basePermalink = $postTypeObject->rewrite ?
            $postTypeObject->rewrite['slug'] :
            $slug;
        $filteredPermalink = '';

        switch ($slug) {
            case 'post':
                $filteredPermalink = str_replace(
                    '/',
                    '',
                    str_replace(
                        '%postname%',
                        '',
                        $wp_rewrite->permalink_structure,
                    ),
                );

                break;

            case 'page':
                $filteredPermalink = '';

                break;

            default:
                $filteredPermalink = $basePermalink;

                break;
        }

        return $filteredPermalink;
    }
}
