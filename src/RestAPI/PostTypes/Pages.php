<?php

namespace Jabbado\RestAPI\PostTypes;

use WordPressPluginAPI\FilterHook;
use Jabbado\RestAPI\Helpers\Params;

class Pages implements FilterHook
{
    /**
     * The post type name
     */
    private static $postType = 'page';

    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        $postType = static::$postType;

        return [
            'rest_' . $postType . '_query' => ['editQuery', 10, 2],
        ];
    }

    /**
     * Add parent slug to arguments for better response
     */
    public function editQuery(
        array $args,
        \WP_REST_Request $request,
    ): array {
        // Get all parameters
        $params = new Params($args, $request);
        $params->setPostParent(static::$postType);

        return $params->args;
    }
}
