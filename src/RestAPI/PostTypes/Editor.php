<?php

namespace Jabbado\RestAPI\PostTypes;

use Jabbado\RestAPI\Helpers\Blocks;
use Jabbado\CustomFields\PostTypes;
use WordPressClassHelpers\RestAPI\CustomFields;

class Editor extends CustomFields
{
    /**
     * ID of the current post
     */
    private $postId;

    /**
     * ID of the current post
     */
    private $postType;

    /**
     * ID of the current post
     */
    private $request;

    /**
     * Register Gutenberg blocks as API field
     */
    public function registerFields()
    {
        $postTypes = get_post_types_by_support(['editor']);

        foreach ($postTypes as $postType) {
            register_rest_field(
                $postType,
                'clean_blocks',
                [
                    'get_callback' => function (
                        array $post,
                        string $attr,
                        \WP_REST_Request $request,
                    ) {
                        $blocks = [];

                        if (array_key_exists('id', $post)) {
                            $this->request = $request;
                            $this->postId = $post['id'];
                            $this->postType = get_post_type($this->postId);

                            $blocks = $this->getBlocks();
                        }

                        return $blocks;
                    }
                ]
            );

            register_rest_field(
                $postType,
                'overview',
                [
                    'get_callback' => function (
                        array $post,
                        string $attr,
                        \WP_REST_Request $request,
                    ) {
                        $data = [];

                        if (array_key_exists('id', $post)) {
                            $this->postId = $post['id'];
                            $data = $this->getOverviewData();
                        }

                        return $data;
                    }
                ]
            );

            register_rest_field(
                $postType,
                'taxonomies',
                [
                    'get_callback' => function (array $post) {
                        $taxonomies = [];

                        if (array_key_exists('id', $post)) {
                            $this->postId = $post['id'];
                            $taxonomies = $this->getPostTerms();
                        }

                        return $taxonomies;
                    }
                ],
            );
        }
    }

    /**
     * Register Gutenberg blocks as API field for preview
     */
    public function registerPreviewFields(
        \WP_REST_Response $response,
        \WP_Post $post,
        \WP_Rest_Request $request,
    ): \WP_REST_Response {
        $data = $response->get_data();

        // Original post ID
        $this->postId = $data['parent'];
        $this->postType = get_post_type($this->postId);
        $this->request = $request;

        // Post types
        $postTypes = get_post_types_by_support(['editor']);

        // Add general API data if correct post type
        if (in_array($this->postType, $postTypes) && function_exists('get_fields')) {
            $data['acf'] = get_fields($this->postId);
            $data['cleanBlocks'] = $this->parseBlocks($data['content']['raw']);
            $data['overview'] = $this->getOverviewData($this->postId);
        }

        $response->set_data($data);

        return $response;
    }

    /**
     * Get the blocks field
     */
    private function getBlocks(): array
    {
        $content = get_post_field('post_content', $this->postId);

        return $this->parseBlocks($content);
    }

    /**
     * Get the blocks field
     */
    private function parseBlocks(string $content): array
    {
        $parseBlocks = parse_blocks($content);

        $blocks = new Blocks(
            $this->postId,
            $this->postType,
            $this->request,
            $parseBlocks,
        );
        $blocks->clean();

        return $blocks->blocks;
    }

    /**
     * Get all the terms
     */
    private function getOverviewData(): array
    {
        $customFields = new PostTypes();

        return $customFields->getOverviewData($this->postId);
    }

    /**
     * Get all the terms
     */
    private function getPostTerms(): array
    {
        $customFields = new PostTypes();

        return $customFields->getPostTerms($this->postId);
    }
}
