<?php

/**
 * Create labels and arguments for a unit test post type registration
 *
 * PHP version 8
 *
 * @package BlockUnitTest\PostTypes\UnitTests
 * @author  SchaapOntwerpers <info@schaapontwerpers.nl>
 * @license https://opensource.org/licenses/GPL-3.0 GNU General Public License
*/
namespace BlockUnitTest\PostTypes;

use WordPressClassHelpers\Register\PostType;

class UnitTests extends PostType
{
    /**
     * Set arguments
     *
     * @return void
     */
    protected function setArgs()
    {
        $this->args = [
            'can_export' => true,
            'capability_type' => 'page',
            'description' => __('All custom block unit tests', 'jabbado'),
            'has_archive' => false,
            'hierarchical' => true,
            'labels' => $this->labels,
            'menu_icon' => 'dashicons-superhero',
            'public' => true,
            'query_var' => true,
            'rest_base' => 'unittests',
            'rewrite' => [
                'with_front' => false,
                'slug' => 'unittests',
            ],
            'show_in_admin_bar' => true,
            'show_in_menu' => true,
            'show_in_rest' => true,
            'supports' => array('title', 'editor'),
        ];
    }

    /**
     * Set labels
     *
     * @return void
     */
    protected function setLabels()
    {
        $this->labels = [
            'add_new' => __('Add Unit Test', 'jabbado'),
            'add_new_item' => __('Add Unit Test', 'jabbado'),
            'edit_item' => __('Edit Unit Test', 'jabbado'),
            'name' => 'Custom Blocks Unit Tests',
            'menu_name' => 'Custom Blocks Unit Tests',
            'new_item' => __('New Unit Test', 'jabbado'),
            'not_found' => __('No unit test found.', 'jabbado'),
            'not_found_in_trash' => __('No unit test found in Trash.', 'jabbado'),
            'search_items' => __('Search unit tests', 'jabbado'),
            'singular_name' => __('unittest', 'jabbado'),
            'view_item' => __('View Unit Test ', 'jabbado'),
        ];
    }

    /**
     * Name of the post type to register
     *
     * @return void
     */
    protected function setName()
    {
        $this->name = 'unittest';
    }
}
