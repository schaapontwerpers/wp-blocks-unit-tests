<?php
/*
Plugin Name:  Block Unit Tests
Plugin URI:   https://packagist.org/packages/schaapdigitalcreatives/wp-core
Description:  The Unit Tests WordPress plugin for Schaap Digital Creatives
Version:      1.0.0
Author:       Schaap Digital Creatives
Author URI:   https://schaapontwerpers.nl/
License:      GNU General Public License
*/

new BlockUnitTest\Init();
